import 'dart:async';

import 'package:attendance_register/Student.dart';
import 'package:attendance_register/storage.dart';
import 'package:flutter/material.dart';
import 'package:pie_chart/pie_chart.dart';
import 'Class.dart';

class DetailsPage extends StatefulWidget {
  DetailsPage({this.student, this.cls});
  final Student student;
  final Class cls;
  int count;
  int totalCount;
  var percent = 0.0;
  Map<String,double> dataMap = new Map();
  @override
  _DetailsPageState createState() => _DetailsPageState();
}

class _DetailsPageState extends State<DetailsPage> {
  @override
  Widget build(BuildContext context) {
    // checkAttendLog();
    Storage().getAttendedNoSessions(widget.student).then((value) {
      setState(() {
        widget.count = value;
        widget.percent = widget.count * 100 / widget.totalCount;
        widget.dataMap.putIfAbsent("Attended", ()=>value.toDouble());
        widget.dataMap.putIfAbsent("Absent",()=> widget.totalCount - value.toDouble());
      });
    });
    Storage().getTotalNoSessions(widget.cls).then((value) {
      setState(() {
        widget.totalCount = value;
      });
    });
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.student.name),
      ),
      body: Container(
        child: Column(
          children: <Widget>[
            Text('Total sessions till now : ${widget.totalCount}'),
            Text('Attended sessions till now : ${widget.count}'),
            Text('Percentage : ${widget.percent}%'),
            Pie(widget.dataMap)
          ],
        ),
      ),
    );
  }
}

class Pie extends StatefulWidget {
  Pie(this.dataMap);
  Map<String, double> dataMap;
  List<Color> colorList = [Colors.green,Colors.red];
  @override
  _PieState createState() => _PieState();
}

class _PieState extends State<Pie> {
  @override
  Widget build(BuildContext context) {
    return PieChart(dataMap: widget.dataMap,colorList: widget.colorList,decimalPlaces: 1);
  }
}
