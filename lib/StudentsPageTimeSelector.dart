import 'dart:developer';

import 'package:attendance_register/Class.dart';
import 'package:attendance_register/StudentsPage.dart';
import 'package:attendance_register/storage.dart';
import 'package:flutter/material.dart';
import 'package:attendance_register/DetailsPage.dart';
import 'package:datetime_picker_formfield/time_picker_formfield.dart';
import 'package:intl/intl.dart';

import 'Log.dart';

class StudentsPageTimeSelector extends StatefulWidget {
  StudentsPageTimeSelector(this.cls);
  final Class cls;
  @override
  _StudentsPageTimeSelectorState createState() =>
      _StudentsPageTimeSelectorState();
}

class _StudentsPageTimeSelectorState extends State<StudentsPageTimeSelector> {
  final startTimeController = TextEditingController();
  final endTimeController = TextEditingController();
  final startTimeFocus = FocusNode();
  final endTimeFocus = FocusNode();
  final timeFormat = DateFormat("h:mm a");

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.cls.name),
      ),
      body: AlertDialog(
        title: Column(
          children: <Widget>[
            TimePickerFormField(
              controller: startTimeController,
              format: timeFormat,
              decoration: InputDecoration(labelText: 'class starting time'),
              initialTime: TimeOfDay.now(),
            ),
            TimePickerFormField(
              controller: endTimeController,
              format: timeFormat,
              decoration: InputDecoration(labelText: 'class ending time'),
              initialTime: TimeOfDay.now(),
            ),
          ],
        ),
        actions: <Widget>[
          FlatButton(
            child: Text('SELECT'),
            onPressed: () async {
              Log log = Log();
              log.startTime = startTimeController.text;
              log.endTime = endTimeController.text;
              var id = await Storage().saveLog(log,widget.cls);
              log.Id = id;
              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(
                      builder: (context) => StudentsPage(widget.cls, log)),
                  (r) => false);
            },
          ),
        ],
      ),
    );
  }
}
