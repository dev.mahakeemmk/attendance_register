import 'package:attendance_register/Class.dart';
import 'package:attendance_register/Student.dart';
import 'package:attendance_register/storage.dart';
import 'package:flutter/material.dart';
import 'package:attendance_register/DetailsPage.dart';
import 'AttendanceLog.dart';
import 'Log.dart';

class StudentsPage extends StatefulWidget {
  final Class cls;
  final Log log;
  final AttendanceLog attendance = AttendanceLog();

  StudentsPage(this.cls, this.log);
  @override
  _StudentsPageState createState() => _StudentsPageState();
}

class _StudentsPageState extends State<StudentsPage> {
  final addStudentFieldController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    widget.log.date = DateTime.now().toString();
    widget.attendance.logId = widget.log.Id;
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.cls.name),
        ),
        body: Container(
          child: ListView.builder(
            itemCount: widget.cls.students.length,
            itemBuilder: (BuildContext context, int index) {
              return Card(
                child: ListTile(
                  title: Row(
                    children: <Widget>[
                      Container(
                          width: 270.0,
                          child: Text(widget.cls.students[index].name)),
                      Checkbox(
                        onChanged: (bool value) {
                          setState(() {
                            widget.attendance.studentId =
                                widget.cls.students[index].Id;
                            widget.attendance.attendance = value;
                            widget.cls.students[index].attendance = value;
                            Storage().saveAttendance(widget.attendance);
                          });
                        },
                        value: widget.cls.students[index].attendance,
                      )
                    ],
                  ),
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => DetailsPage(
                                  student: widget.cls.students[index],
                                  cls: widget.cls,
                                )));
                  },
                ),
              );
            },
          ),
        ),
        floatingActionButton: FloatingActionButton(
          tooltip: 'ADD',
          child: Icon(Icons.add),
          onPressed: () {
            _addStudentDialogue();
          },
        ));
  }

  Future<void> _addStudentDialogue() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return AlertDialog(
          content: TextFormField(
            controller: addStudentFieldController,
            decoration: InputDecoration(labelText: 'Enter new student name'),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('ADD'),
              onPressed: () async {
                var student = Student(name: addStudentFieldController.text);
                var id = await Storage().saveStudent(student, widget.cls);
                setState(() {
                  widget.cls.students.add(
                      Student(Id: id, name: addStudentFieldController.text));
                });
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
