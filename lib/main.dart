import 'package:attendance_register/Class.dart';
import 'package:attendance_register/Student.dart';
import 'package:attendance_register/storage.dart';
import 'package:flutter/material.dart';
import 'StudentsPageTimeSelector.dart';

List<Class> allClasses;

void main() async {
  await initData();
  runApp(MyApp());
  }
void initData() async {
  Storage storage = new Storage();
  allClasses = await storage.getClasses() ?? new List();
}
class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Attendace Register',
      theme: ThemeData(
        primarySwatch: Colors.deepPurple,
      ),
      home: MyHomePage(title: 'ALL CLASSES'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  
  final addClassFieldController = TextEditingController();

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    addClassFieldController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Container(
        child: ListView.builder(
          itemCount: allClasses.length,
          itemBuilder: (BuildContext context, int index) {
            return Card(
              child: ListTile(
                title: Text(allClasses[index].name),
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) =>
                              StudentsPageTimeSelector(allClasses[index])));
                },
              ),
            );
          },
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          _addClassDialogue();
          addClassFieldController.clear();
        },
        tooltip: 'Add',
        child: Icon(Icons.add),
      ),
    );
  }

  Future<void> _addClassDialogue() async {
    List<Student> stdEmpty = List();
    return showDialog<void>(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return AlertDialog(
          content: TextFormField(
            controller: addClassFieldController,
            decoration: InputDecoration(labelText: 'Enter new class name'),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('ADD'),
              onPressed: () async {
                var cls = Class(name: addClassFieldController.text,students: stdEmpty);
                var id =  await Storage().saveClass(cls);
                cls.id = id;
                setState(() {
                  allClasses.add(cls);
                });
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
