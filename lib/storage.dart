import 'dart:core';
import 'dart:core' as prefix0;

import 'package:attendance_register/AttendanceLog.dart';
import 'package:attendance_register/Class.dart';
import 'package:attendance_register/Student.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

import 'Log.dart';

class Storage {
  static Database _db;
  Future<Database> get db async {
    if (_db != null) {
      return _db;
    }
    _db = await initDb();
    return _db;
  }

  initDb() async {
    var databasesPath = await getDatabasesPath();
    String path = join(databasesPath, 'register.db');
    var db = await openDatabase(path, version: 1, onCreate: _onCreate);
    return db;
  }

  void _onCreate(Database db, int newVersion) async {
    await db.execute('CREATE TABLE classes(id INTEGER PRIMARY KEY, name TEXT)');
    await db.execute(
        'CREATE TABLE students(id INTEGER PRIMARY KEY, name TEXT,class_id INTEGER)');
    await db.execute(
        'CREATE TABLE attendance_log(id INTEGER PRIMARY KEY, logId INTEGER,student_id INTEGER,attendance INTEGER)');
    await db.execute(
        'CREATE TABLE log(id INTEGER PRIMARY KEY, date TEXT,start_time TEXT,end_time TEXT,class_id INTEGER)');
  }

  Future<int> saveClass(Class cls) async {
    print('saving class');
    var dbClient = await db;
    return await dbClient
        .rawInsert('INSERT INTO classes (name) VALUES ( \'${cls.name}\')');
  }

  Future<int> saveStudent(Student student, Class cls) async {
    var dbClient = await db;
    var classId = await getClass(cls);
    return await dbClient.rawInsert(
        'INSERT INTO students (name,class_id) VALUES ( \'${student.name}\',\'${classId.id}\')');
  }

  Future<int> saveLog(Log log,Class classId) async {
    var dbClient = await db;
    var id = await dbClient.rawInsert(
        'INSERT INTO log (date,start_time,end_time,class_id) VALUES ( \'${log.date}\',\'${log.startTime}\',\'${log.endTime}\',\'${classId.id}\')');
    return id;
  }

  void saveAttendance(AttendanceLog attendanceLog) async {
    var dbClient = await db;
    var id;
    var log = attendanceLog;
    var logId = attendanceLog.logId;
    int attendance = attendanceLog.attendance == true ? 1 : 0;

    if (! await(isAlreadyExists(attendanceLog.logId,attendanceLog.studentId))) {
      id = dbClient.rawInsert(
          'INSERT INTO attendance_log (logId,student_id,attendance) VALUES ( \'${attendanceLog.logId}\',\'${attendanceLog.studentId}\',\'${attendance}\')');
    } else {
      dbClient.rawUpdate(
          'UPDATE attendance_log SET attendance = ? WHERE logId = ?',
          [attendance, attendanceLog.logId]);
    }
  }

  Future<List<AttendanceLog>> getAttendance() async {
    var dbClient = await db;
    var result = await dbClient.rawQuery('SELECT * FROM attendance_log');
    print(result);
    List<AttendanceLog> list = List();
    for (var item in result) {
      var attendaceLog = AttendanceLog();
      attendaceLog.logId = item['logId'];
      attendaceLog.studentId = item['student_id'];
      list.add(attendaceLog);
    }
    return list;
  }

  Future<int> getAttendedNoSessions(Student student) async {
    var dbClient = await db;
    var count = Sqflite.firstIntValue(await dbClient.rawQuery(
        'SELECT COUNT(*) FROM attendance_log WHERE student_id = ? AND attendance = 1',
        [student.Id]));
    return count;
  }

  Future<int> getTotalNoSessions(Class classId) async {
    var dbClient = await db;
    var count = Sqflite.firstIntValue(await dbClient.rawQuery('SELECT COUNT(*) FROM log WHERE class_id = ?',[classId.id]));
    return count;
  }

  Future<Class> getClass(Class cls) async {
    var dbClient = await db;
    var result = await dbClient
        .rawQuery('SELECT * FROM classes WHERE name = ?', [cls.name]);
    for (var row in result) {
      cls = Class(id: row['id'], name: row['name']);
    }
    return cls;
  }

  Future<List<Class>> getClasses() async {
    var dbClient = await db;
    var result = await dbClient.rawQuery('SELECT * FROM classes');

    List<Class> list = List();
    for (var row in result) {
      list.add(Class(
          id: row['id'],
          name: row['name'],
          students: await getStudents(row['id'])));
    }
    return list;
  }

  Future<List<Student>> getStudents(int classId) async {
    var dbClient = await db;
    var result = await dbClient
        .rawQuery('SELECT * FROM students WHERE class_id = ?', [classId]);
    List<Student> list = List();
    for (var row in result) {
      list.add(Student(Id: row['id'], name: row['name']));
    }
    return list;
  }

  Future<bool> isAlreadyExists(int logId,int studentId) async {
    var attendanceLog = await getAttendance();
    bool isExists = false;
    if(attendanceLog.length > 0) {
      for (int i = 0;i<attendanceLog.length;i++) {
        if ((attendanceLog[i].logId == logId) && (attendanceLog[i].studentId == studentId)) {
          isExists = true;
          break;
        } else {
          isExists = false;
        }
      }
    }
    return isExists;
  }
}
